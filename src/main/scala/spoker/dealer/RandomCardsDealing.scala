package spoker.dealer

import spoker.{Card, Rank, Suit}

import scala.collection.SortedSet
import scala.util.Random

class RandomCardsDealing extends CardsDealing {

  def randomDeck: List[Card] = {
    val deck: SortedSet[Card] = for {
      suit <- Suit.values
      rank <- Rank.values
    } yield new Card(rank, suit)
    Random.shuffle(deck.toList)
  }

  val deck: List[Card] = randomDeck
  var currentCard = 0

  override def nextCardTo(target: DealtCardTarget): Card = {
    val card = deck(currentCard)
    currentCard = currentCard + 1
    card
  }
}
