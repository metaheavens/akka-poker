package routing

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import org.json4s.DefaultFormats
import spoker.{Player, Table}
import org.json4s._
import scalaz.{-\/, \/, \/-}
import spoker.betting.PositionedPlayer
import spoker.dealer.RandomCardsDealing

import scala.collection.mutable

object Routes extends Directives with Json4sSupport {
  implicit val serialization = native.Serialization // or native.Serialization
  implicit val formats = DefaultFormats

  var nextId = 0
  val gameList: mutable.Map[Int, Table] = mutable.Map()

  type PlayerID = BigInt

  case class CreateGameOrder(players: List[PlayerID])

  val route: Route =
    get {
      pathPrefix("item" / LongNumber) { id =>
        complete(StatusCodes.OK)
      }
    } ~
      post {
        path("game") {
          entity(as[CreateGameOrder]) { order =>
            val playerList: FailureReason \/ List[PositionedPlayer] = generatePlayers(order.players)
            playerList match {
              case -\/(reason) => complete(reason, StatusCodes.BadRequest)
              case \/-(playerList) =>
                val gameId = nextId
                nextId = nextId + 1
                val table = Table(playerList, cardsDealing = new RandomCardsDealing)
                gameList.put(gameId, table)
                complete(gameId.toString)
            }
          }
        }
      }

  def generatePlayers(ids: List[PlayerID],
                      initialStack: Int = 1500): FailureReason \/ List[PositionedPlayer] =
    ids match {
      case firstId :: ids =>
        val firstPlayer = PositionedPlayer(Player(firstId.toString), initialStack, isButton = true)
        val players = ids map (id => PositionedPlayer(Player(id.toString), initialStack))
        \/-(firstPlayer +: players)
      case _ => -\/(FailureReason("There should be at least 2 players to start a game"))
    }

  case class FailureReason(msg: String)

}
